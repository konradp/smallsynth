'use strict';

class Synth {
  constructor() {
    this.noteFreqs = []
    this.oscList = [];
    this.mainGainNode = null;
    try {
      // Init AudioContext
      this.context = new (window.AudioContext || window.webkitAudioContext)();
    } catch(e) {
      throw 'Web Audio API is not supported in this browser';
    }
    this.createNoteTable();
    this.initOscilators();
  }


  // PUBLIC
  setGain(gain) {
    // min: 0, max: 1
    this.mainGainNode.gain.value = gain;
  }


  midiNoteOn(midiKey, velocity) {
    let freq = this.noteFreqs[midiKey];
    let osc = this.oscList[midiKey].osc;
    osc.frequency.value = freq;
    this.oscList[midiKey].gainNode.gain.value = velocity/127;
  }


  midiNoteOff(midiKey) {
    this.oscList[midiKey].gainNode.gain.value = 0;
  }




  //// PRIVATE
  initOscilators() {
    // Main volume
    this.mainGainNode = this.context.createGain();
    this.mainGainNode.connect(this.context.destination);
    this.mainGainNode.gain.value = 0.1;
    for (let i=0; i<127; i++) {
      // Notes that can play, each has own oscillator and GainNode (amp env)
      let gainNode = this.context.createGain();
      gainNode.gain.value = 0;
      gainNode.connect(this.mainGainNode);
      let osc = this.context.createOscillator();
      osc.type = 'sine';
      osc.connect(gainNode);
      osc.start();
      this.oscList[i] = {
        osc: osc,
        gainNode: gainNode,
      };
    }
  }


  createNoteTable() {
    for (var i = 0; i <= 127; i++) {
      this.noteFreqs.push(+this.midiToFreq(i).toFixed(2));
    }
  }


  midiToFreq(midiNote) {
    // P_n = 440*2^((n-49)/12)
    // Ref: https://en.wikipedia.org/wiki/Equal_temperament#Calculating_absolute_frequencies
    // Ref: https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
    let freq = 440 * ( Math.pow(2, (midiNote-69)/12) );
    return freq;
  }
}; // class Player
