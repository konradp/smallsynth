// Adapted from
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth
// https://developer.mozilla.org/en-US/docs/Web/API/Web_MIDI_API
// TODO: See this: https://webaudio.github.io/web-midi-api/#a-simple-monophonic-sine-wave-midi-synthesizer

let midi = null;  // global MIDIAccess object
let synth = null; // import synth.js
let debug = true;
let midiInitialised = false;
let keysPressed = 0;
window.addEventListener('load', Run);

// MAIN
function Run() {
  document.body.addEventListener('click', () => {
    if (!midiInitialised) {
      navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
      volCtl = document.querySelector("input[name='volume']");
      volCtl.addEventListener('input', changeVolume, false);
    }
    midiInitialised = true;
  });
}


function onMIDISuccess(midiAccess) {
  // MIDI has been initialised
  console.log('MIDI initialised');
  midi = midiAccess;
  uiSetMidiDevicesList();
  startLoggingMIDIInput(midi);
  synth = new Synth();
}


function onMIDIFailure(msg) {
  console.log("Failed to get MIDI access - " + msg);
}


navigator.requestMIDIAccess()
  // Device listener
  .then(function(access) {
    // Get lists of available MIDI controllers
    const inputs = access.inputs.values();
    const outputs = access.outputs.values();
    access.onstatechange = function(e) {
      // Print information about the (dis)connected MIDI controller
      uiSetMidiDevicesList();
      console.log('New device:', {
        'e.port.name': e.port.name,
        'e.port.manufacturer': e.port.manufacturer,
        'e.port.state': e.port.state
      });
    };
  });


function onMIDIMessage(event) {
  // Event types:
  // - 144: note on
  // - 128: note off
  let eve = event.data[0];
  let key = event.data[1];
  let vel = event.data[2];
  if (debug) {
    console.log('EVENT', eve, key, vel);
  }
  if (eve == 144) {
    synth.midiNoteOn(key, vel);
  } else if (eve == 128) {
    synth.midiNoteOff(key);
  }
}


function startLoggingMIDIInput(midiAccess, indexOfPort) {
  midiAccess.inputs.forEach( function(entry) {entry.onmidimessage = onMIDIMessage;});
}


function uiSetMidiDevicesList() {
  var midi_devices = document.querySelector('#midi-devices');
  midi_devices.innerHTML = '';
  var i = 0;
  for (var entry of midi.inputs) {
    i++;
    var input = entry[1];
    var option = document.createElement('option');
    //option.value = input.id;
    option.innerHTML = input.name;
    option.value = input.id;
    if (i == midi.inputs.size) {
      option.selected = true;
    }
    midi_devices.appendChild(option);
  }
}


function changeVolume(ev) {
  synth.setGain(this.value);
  console.log('vol', ev, this.value);
}
