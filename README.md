# smallsynth
<https://konradp.gitlab.io/smallsynth>

## Run
```BASH
npm install
npm start
```
Navigate in web browser to http://localhost:3000/
